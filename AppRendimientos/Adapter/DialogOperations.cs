﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Database;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using AppRendimientos.Class;

namespace AppRendimientos.Adapter
{
    class DialogOperations : DialogFragment {

        Context context;
        ConexionLocal db;
        View view;
        TextView txtHour;
        TextView txtBusinessUnit;
        Button btnSave;
        Button btnCancel;
        string title;
        int businessUnitID;
        int bouquetTypeID;

        public DialogOperations(Context context, string title, int businessUnitID, int bouquetTypeID)
        {
            this.context = context;
            this.title = title;
            this.businessUnitID = businessUnitID;
            this.bouquetTypeID = bouquetTypeID;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.DialogOperations, container, false);
            txtHour = view.FindViewById<TextView>(Resource.Id.txtHour);
            btnSave = view.FindViewById<Button>(Resource.Id.btnSave);
            btnCancel = view.FindViewById<Button>(Resource.Id.btnCancel);
            txtBusinessUnit = view.FindViewById<TextView>(Resource.Id.txtBusinessUnit);

            txtBusinessUnit.Text = title;
            txtHour.Text = DateTime.Now.ToString();

            txtHour.Click += TxtDate_Click;
            btnCancel.Click += BtnCancel_Click;
            btnSave.Click += BtnSave_Click;
            return view;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            db = new ConexionLocal(Application.Context);
            db.SaveOperationStartDatetime(businessUnitID, bouquetTypeID, txtHour.Text);
            Toast.MakeText(Application.Context, "Hora guardada", ToastLength.Long).Show();
            this.Dismiss();
        }

        private void TxtDate_Click(object sender, EventArgs e)
        {
            TimePickerFragment frag = TimePickerFragment.NewInstance(
                 delegate (DateTime time)
                 {
                     txtHour.Text = DateTime.Now.ToShortDateString() +" " + time.ToLongTimeString();
                 });

            frag.Show(FragmentManager, TimePickerFragment.TAG);
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {           
            this.Dismiss();
        }
    }
}