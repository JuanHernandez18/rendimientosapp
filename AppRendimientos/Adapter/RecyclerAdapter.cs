﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using AppRendimientos.Class;

namespace AppRendimientos.Adapter
{
    public class RecyclerViewHolder : RecyclerView.ViewHolder, View.IOnClickListener, View.IOnLongClickListener, ILocationListener
    {
        public TextView txtNombre;
        private IItemClickListenener itemClickListener;
        Location currentLocation;
        double Latitude;
        double Longitude;

        public RecyclerViewHolder(View itemView, LayoutInflater inflater) : base(itemView)
        {
            txtNombre = itemView.FindViewById<TextView>(Resource.Id.txtNombre);

            itemView.SetOnClickListener(this);
            itemView.SetOnLongClickListener(this);

        }

        public void SetItemClickListener(IItemClickListenener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }

        public void OnClick(View v)
        {
            v.SetBackgroundColor(Android.Graphics.Color.Transparent);
            itemClickListener.OnClick(v, AdapterPosition, false);
        }

        public bool OnLongClick(View v)
        {
            itemClickListener.OnClick(v, AdapterPosition, true);
            return true;
        }

        public void OnLocationChanged(Location location)
        {
            currentLocation = location;
            if (currentLocation != null)
            {
                Latitude = currentLocation.Latitude;
                Longitude = currentLocation.Longitude;
            }
        }

        public void OnProviderDisabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnProviderEnabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
            throw new NotImplementedException();
        }
    }
    public class RecyclerAdapter : RecyclerView.Adapter, IItemClickListenener
    {
        List<ListData> lstdata;
        private Context context;
        RecyclerViewHolder vh;
        View itemView;
        LayoutInflater inflater;
        private SparseBooleanArray selecteditems = new SparseBooleanArray();
        int tipo;
        LocationManager locationManager;
        string provider;
        Location lm;
        double Latitude;
        double Longitude;

        public RecyclerAdapter(List<ListData> list, Context context, int tipo)
        {
            this.lstdata = list;
            this.context = context;
            this.tipo = tipo;
        }

        public override int ItemCount
        {
            get
            {
                return lstdata.Count;
            }
        }

        public override int GetItemViewType(int position)
        {
            int value = 0;
            if (lstdata[position].estado == 1)
            {
                value = 1;
            }
            
            return value;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            vh = holder as RecyclerViewHolder;
            vh.txtNombre.Text = lstdata[position].nombre;

            int type = GetItemViewType(position);

            if (type == 1)
            {
                vh.txtNombre.SetBackgroundResource(Android.Graphics.Color.Green);
            }
            vh.SetItemClickListener(this);
        }

        public void OnClick(View itemView, int position, bool isLongClick)
        {
            if (tipo == 1) {
                    int i = 0;
                    foreach (var item in lstdata)
                    {
                        if (item.estado == 1)
                        {
                            i = i + 1;
                        }
                    }

                    if (i >= 1)
                    {
                        var a = lstdata.Count;

                        for (int x = 0; x < lstdata.Count; x++)
                        {
                            lstdata[x].estado = 0;
                        }
                    }
                    lstdata[position].estado = 1;
            }
        }
        


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            inflater = LayoutInflater.From(parent.Context);
            
             itemView = inflater.Inflate(Resource.Layout.ListData, parent, false);

            return new RecyclerViewHolder(itemView, inflater);
        }
    }

}