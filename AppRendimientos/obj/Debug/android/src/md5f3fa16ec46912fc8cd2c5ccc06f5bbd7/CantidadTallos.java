package md5f3fa16ec46912fc8cd2c5ccc06f5bbd7;


public class CantidadTallos
	extends android.app.Fragment
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"n_onCreateView:(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;:GetOnCreateView_Landroid_view_LayoutInflater_Landroid_view_ViewGroup_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("AppRendimientos.Fragments.CantidadTallos, AppRendimientos, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CantidadTallos.class, __md_methods);
	}


	public CantidadTallos ()
	{
		super ();
		if (getClass () == CantidadTallos.class)
			mono.android.TypeManager.Activate ("AppRendimientos.Fragments.CantidadTallos, AppRendimientos, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public CantidadTallos (android.widget.EditText p0, java.lang.String p1)
	{
		super ();
		if (getClass () == CantidadTallos.class)
			mono.android.TypeManager.Activate ("AppRendimientos.Fragments.CantidadTallos, AppRendimientos, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Widget.EditText, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.String, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1 });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);


	public android.view.View onCreateView (android.view.LayoutInflater p0, android.view.ViewGroup p1, android.os.Bundle p2)
	{
		return n_onCreateView (p0, p1, p2);
	}

	private native android.view.View n_onCreateView (android.view.LayoutInflater p0, android.view.ViewGroup p1, android.os.Bundle p2);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
