﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using AppRendimientos.Class;

namespace AppRendimientos.Fragments
{
    public class Colaborador : Fragment
    {
        EditText etColaborator;
        CantidadTallos FgCatidadTallos;
        ConexionLocal db;
        Fragment mCurrentFragment;
      

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.FgColaborador, container, false);

            db = new ConexionLocal(Application.Context);
            etColaborator = view.FindViewById<EditText>(Resource.Id.etColaborator);
            etColaborator.RequestFocus();

            etColaborator.TextChanged += (object senderc, Android.Text.TextChangedEventArgs ev) =>
            {
                if (etColaborator.Text.Length == 6)
                {
                    FgCatidadTallos = new CantidadTallos(etColaborator, etColaborator.Text);
                    ReplaceFragment(FgCatidadTallos);
                }
                


                //DateTime fechaM = DateTime.Now;
                //        String DateRegister = string.Format("{0:yyyy/MM/dd HH:mm:ss}", fechaM);

                //        db.SavePerformance(businessUnitID, plantProductID, productID, colourID, quantityStemID, quantityBqtID, qualityID, etColaborator.Text, DateRegister, 0);
                //        Toast.MakeText(Application.Context, "Registro realizado", ToastLength.Short).Show();

                                                       
            };
            return view;
        }


        public void ReplaceFragment(Fragment fragment)
        {
            if (fragment.IsVisible)
            {
                return;
            }
            var trans = FragmentManager.BeginTransaction();
            trans.Replace(Resource.Id.fragmentContainer, fragment);
            trans.AddToBackStack(null);
            trans.Commit();

            mCurrentFragment = fragment;

        }
    }
}

