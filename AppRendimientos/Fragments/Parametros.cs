﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using AppRendimientos.Adapters;
using AppRendimientos.Class;

namespace AppRendimientos.Fragments
{
    public class Parametros : Fragment
    {
        View view;
        List<ListBD> lstBDProduct = new List<ListBD>();
        List<ListBD> lstBDBusiness = new List<ListBD>();
        List<ListBD> lstBDBouquetType = new List<ListBD>();


        Fragment mCurrentFragment;
        Calidad FgCalidad;
        TextView txtTitulo;
        ConexionLocal db;
        Spinner spProduct;
        Spinner spBusinessUnit;
        Spinner spBouquetID;
        ArrayAdapter adapter;
        Button btnGuardar;

        Switch swChange;

        int ProductID;
        int BusinessUnitID;
        int BouquetTypeID;
        int ParameterID;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.FgParametros, container, false);
            spBusinessUnit = view.FindViewById<Spinner>(Resource.Id.spBusinessUnitID);
            spProduct = view.FindViewById<Spinner>(Resource.Id.spProductID);
            spBouquetID = view.FindViewById<Spinner>(Resource.Id.spBouquetID);

            btnGuardar = view.FindViewById<Button>(Resource.Id.btnSave);

            txtTitulo = (TextView)view.FindViewById(Resource.Id.txtTitulo);
            txtTitulo.Text = "Parametrización";
           
            cargarUnidades();
            cargarProducto();
            cargarTipo();

            btnGuardar.Click += BtnGuardar_Click;
           // swChange.CheckedChange += SwChange_CheckedChange; ;
            return view;
        }

        private void cargarTipo()
        {
            db = new ConexionLocal(Application.Context);

            lstBDBouquetType.Clear();

            ICursor cursorData = db.queryBouquetType();
            if (cursorData.MoveToFirst())
            {
                do
                {
                    lstBDBouquetType.Add(new ListBD() { ID = cursorData.GetInt(0), Name = cursorData.GetString(1) });
                } while (cursorData.MoveToNext());
            }

            adapter = new ArrayAdapter(Activity, Android.Resource.Layout.SimpleListItem1, lstBDBouquetType);
            spBouquetID.Adapter = adapter;

            spBouquetID.ItemSelected += SpBouquetID_ItemSelected; ;
        }

        private void SpBouquetID_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstBDBouquetType[e.Position];
            BouquetTypeID = l.ID;
        }

        private void SwChange_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            if (swChange.Checked == true)
            {
                VariablesGlobales.vgTipoLectura = 1;
            }
            else
            {
                VariablesGlobales.vgTipoLectura = 0;
            }
        }        

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            db = new ConexionLocal(Application.Context);
            
            ICursor cursorData = db.queryParameters();
            
            if(cursorData.Count >= 1)
            {                
                db.DeleteParameters();
                db.SaveParameters(BusinessUnitID, ProductID, BouquetTypeID);
            }
            else
            {
                db.SaveParameters(BusinessUnitID, ProductID, BouquetTypeID);
            }
           
            Toast.MakeText(Application.Context, "ParametrosGuardados", ToastLength.Short).Show();
        }

        private void cargarUnidades()
        {
            db = new ConexionLocal(Application.Context);

            lstBDBusiness.Clear();

            ICursor cursorData = db.queryBusinessUnit();
            if (cursorData.MoveToFirst())
            {
                do
                {
                    lstBDBusiness.Add(new ListBD() { ID = cursorData.GetInt(0), Name = cursorData.GetString(1) });
                } while (cursorData.MoveToNext());
            }

            adapter = new ArrayAdapter(Activity, Android.Resource.Layout.SimpleListItem1, lstBDBusiness);
            spBusinessUnit.Adapter = adapter;

            spBusinessUnit.ItemSelected += SpBusinessUnit_ItemSelected;
        }

        private void SpBusinessUnit_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstBDBusiness[e.Position];
            BusinessUnitID = l.ID;
        }

        private void cargarProducto()
        {
            db = new ConexionLocal(Application.Context);

            lstBDProduct.Clear();

            ICursor cursorData = db.queryPlantProduct();
            if (cursorData.MoveToFirst())
            {
                do
                {
                    lstBDProduct.Add(new ListBD() { ID = cursorData.GetInt(0), Name = cursorData.GetString(1)});
                } while (cursorData.MoveToNext());
            }

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstBDProduct);

            spProduct.Adapter = adapter;
            spProduct.ItemSelected += SpProduct_ItemSelected;
        }

        private void SpProduct_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstBDProduct[e.Position];
            ProductID = l.ID;
        }

        public void ReplaceFragment(Fragment fragment)
        {
            if (fragment.IsVisible)
            {
                return;
            }
            var trans = FragmentManager.BeginTransaction();
            trans.Replace(Resource.Id.fragmentContainer, fragment);
            trans.AddToBackStack(null);
            trans.Commit();

            mCurrentFragment = fragment;
        }
    }
}