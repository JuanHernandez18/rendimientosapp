﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using AppRendimientos.Adapters;
using AppRendimientos.Class;
using ZXing.Mobile;

namespace AppRendimientos.Fragments
{
    public class CantidadTallos : Fragment
    {
        View view;
        GridView gCantidad;
        List<ListData> lstData = new List<ListData>();
        Fragment mCurrentFragment;
        TextView txtTitulo;
        ConexionLocal db;
        EditText etColaborator;
        string textColaborador;
        Calidad FgCalidad;
        int businessUnitID = 0;
        int PlantProductID = 0;

        public CantidadTallos(EditText etColaborator, string textColaborador)
        {
            this.etColaborator = etColaborator;
            this.textColaborador = textColaborador;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.FgData, container, false);
            txtTitulo = (TextView)view.FindViewById(Resource.Id.txtTitulo);
            gCantidad = (GridView)view.FindViewById(Resource.Id.gData);
            gCantidad.NumColumns = 2;
            txtTitulo.Text = "Selección de Cantidad Tallos";

            gCantidad.ItemClick += GCantidad_ItemClick;

            db = new ConexionLocal(Application.Context);
            ICursor cursorData = db.getParameterUnit();
            if (cursorData.MoveToFirst())
            {
                do
                {
                    businessUnitID = cursorData.GetInt(0);
                    PlantProductID = cursorData.GetInt(1);
                } while (cursorData.MoveToNext());
            }
            cargarCantidad();
            return view;
        }

        private void GCantidad_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var l = lstData[e.Position];
            VariablesGlobales.vgQtyStemID = l.id;
            //DateTime fechaM = DateTime.Now;
            //String DateRegister = string.Format("{0:yyyy/MM/dd HH:mm:ss}", fechaM);
            //db = new ConexionLocal(Application.Context);
            //db.SavePerformance(textColaborador, l.id, DateRegister, 0);

            FgCalidad = new Calidad(etColaborator, textColaborador, l.id);            
            ReplaceFragment(FgCalidad);

        }

        private void cargarCantidad()
        {
            db = new ConexionLocal(Application.Context);
            lstData.Clear();

            ICursor cursorData = db.queryQtyStem(PlantProductID);
            if (cursorData.MoveToFirst())
            {
                do
                {
                    lstData.Add(new ListData(cursorData.GetInt(0), cursorData.GetString(1), 0));
                } while (cursorData.MoveToNext());
            }

            gCantidad.Adapter = new AdData(Activity, lstData);
            //db = new ConexionLocal(Application.Context);

            //lstData.Clear();

            //lstData.Add(new ListData(3, "X3", 0));
            //lstData.Add(new ListData(4, "X4", 0));
            //lstData.Add(new ListData(5, "X5", 0));
            //lstData.Add(new ListData(6, "X6", 0));
            //lstData.Add(new ListData(12, "X12", 0));
            //lstData.Add(new ListData(24, "X24", 0));
            //lstData.Add(new ListData(30, "X30", 0));

            //gCantidad.Adapter = new AdData(Activity, lstData);
        }

        public void ReplaceFragment(Fragment fragment)
        {
            if (fragment.IsVisible)
            {
                return;
            }
            var trans = FragmentManager.BeginTransaction();
            trans.Replace(Resource.Id.fragmentContainer, fragment);
            trans.AddToBackStack(null);
            trans.Commit();

            mCurrentFragment = fragment;
        }

    }
}