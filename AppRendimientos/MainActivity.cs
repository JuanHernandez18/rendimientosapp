﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using AppRendimientos.Fragments;
using Android.Views;
using AppRendimientos.Task;
using AppRendimientos.Class;
using System.Collections.Generic;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.Design.Widget;
using Android.Database;
using System.Data.SqlClient;
using AppRendimientos.Clases;
using System;
using static Android.Provider.Settings;
using AppRendimientos.Adapter;

namespace AppRendimientos
{
    [Activity(Label = "AppRendimientos", Theme = "@style/AppTheme", MainLauncher = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity
    {
        ProgressDialog progressSync;    
        Colaborador FgColaborador;
        Fragment mCurrentFragment;
        Parametros FgParametros;
        ProgressDialog progress;
        List<ListDownload> lstDownload = new List<ListDownload>();
        List<ListPerformance> lstPerformance = new List<ListPerformance>();
        BottomNavigationView bottomNavigation;
        private ConexionLocal db;
        private SqlConnection conexion;
        GeneraTXT GeneraTXT;
        TextView toolbar_title;
        private object fragmentTransactions;

        int BusinessUnitID;
        int BouquetTypeID;
        string title = "";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

           
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            toolbar_title = FindViewById<TextView>(Resource.Id.toolbar_title);

            
            db = new ConexionLocal(Application.Context);
            ICursor cursorData = db.queryParameters();

            if (cursorData.Count > 0)
            {
                if (cursorData.MoveToFirst())
                {
                    do
                    {
                        title = cursorData.GetString(1) + " - " + cursorData.GetString(0);
                        BusinessUnitID = cursorData.GetInt(2);
                        BouquetTypeID = cursorData.GetInt(3);
                    } while (cursorData.MoveToNext());
                }
            }
            else
            {
                title = "Sin parametrizar";
            }
            

            toolbar_title.Text = title;



            SetSupportActionBar(toolbar);

            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);

            
            FgColaborador = new Colaborador();
            FgParametros = new Parametros();

            var trans = FragmentManager.BeginTransaction();
            trans.Add(Resource.Id.fragmentContainer, FgColaborador, "FgColaborador");
            trans.Commit();

            mCurrentFragment = FgColaborador;

            progress = new ProgressDialog(this);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetMessage("Descargando información. Por favor espere...");
            progress.SetCancelable(false);

            progressSync = new ProgressDialog(this);
            progressSync.Indeterminate = true;
            progressSync.SetProgressStyle(ProgressDialogStyle.Spinner);
            progressSync.SetCancelable(false);


            VariablesGlobales.vgTipoLectura = 0;


            bottomNavigation = FindViewById<BottomNavigationView>(Resource.Id.bottom_navigation);
            bottomNavigation.NavigationItemSelected += BottomNavigation_NavigationItemSelected;
        }

        private void BottomNavigation_NavigationItemSelected(object sender, BottomNavigationView.NavigationItemSelectedEventArgs e)
        {
            switch (e.Item.ItemId)
            {
                case Resource.Id.Inicio:
                    ReplaceFragment(FgColaborador);
                    break;
                case Resource.Id.Carga:
                    db = new ConexionLocal(Application.Context);
                                                         
                    ICursor cursor = db.querySync();
                    ICursor cursorOperation = db.querySyncOperation();
                    string addressIPLocal = "10.99.0.2";
                    string database = "BAS";
                    string user = "PRaut";
                    string password = "Neon001*";

                    string conexionsq = @"data source= " + addressIPLocal + ";initial catalog=" + database + ";user id=" + user + ";password= " + password + ";Connect Timeout=0;"; //;Max Pool Size=50000 Max Pool Size=200;MultipleActiveResultSets=true

                    conexion = new SqlConnection(conexionsq);

                    try
                    {
                        conexion.Open();

                        if (cursor.Count > 0 || cursorOperation.Count > 0)
                        {
                            TaskUpload tarea = new TaskUpload(progressSync, cursor, cursorOperation, conexion);
                            tarea.Execute();
                        }
                        else
                        {
                            Toast.MakeText(Application.Context, "No tiene datos para sincronizar", ToastLength.Long).Show();
                        }
                    }
                    catch (SqlException Exp)
                    {
                        Toast.MakeText(Application.Context, "No estas conectado a internet", ToastLength.Long).Show();
                    }

                    break;
                case Resource.Id.Ajustes:
                    ReplaceFragment(FgParametros);
                    break;
                case Resource.Id.Descarga:

                    lstDownload.Clear();

                    lstDownload.Add(new ListDownload { id = 1, Nombre = "PlantProduct" });
                    lstDownload.Add(new ListDownload { id = 2, Nombre = "Product" });
                    lstDownload.Add(new ListDownload { id = 3, Nombre = "Colour" });
                    lstDownload.Add(new ListDownload { id = 4, Nombre = "Quality" });
                    lstDownload.Add(new ListDownload { id = 5, Nombre = "QtyStem" });
                    lstDownload.Add(new ListDownload { id = 5, Nombre = "QtyBqt" });
                    lstDownload.Add(new ListDownload { id = 6, Nombre = "BusinessUnit" });
                    lstDownload.Add(new ListDownload { id = 7, Nombre = "BuoquetType" });

                    TaskDownloadApp download = new TaskDownloadApp(progress, lstDownload);
                    download.Execute();
                    break;
            }
        }
        

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            var inflater = MenuInflater;
            inflater.Inflate(Resource.Menu.menuOptions, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.OperationStart:
                    FragmentTransaction fragmentTransactions;
                    fragmentTransactions = this.FragmentManager.BeginTransaction();
                    DialogOperations DialogO = new DialogOperations(Application.Context, title, BusinessUnitID, BouquetTypeID);
                    DialogO.Cancelable = false;
                    DialogO.Show(fragmentTransactions, "");
                    return true;                
            }
            return base.OnOptionsItemSelected(item);
        }

        public void ReplaceFragment(Fragment fragment)
        {
            if (fragment.IsVisible)
            {
                return;
            }
            var trans = FragmentManager.BeginTransaction();
            trans.Replace(Resource.Id.fragmentContainer, fragment);
            trans.AddToBackStack(null);
            trans.Commit();

            mCurrentFragment = fragment;
        }
    }
}

