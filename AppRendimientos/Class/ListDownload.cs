﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppRendimientos.Class
{
    class ListDownload
    {
        public int id { get; set; }
        public String Nombre { get; set; }

        public override string ToString()
        {
            return Nombre;
        }

    }
}