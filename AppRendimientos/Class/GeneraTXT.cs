﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppRendimientos.Clases
{
    class GeneraTXT
    {   

        public void GenerarArchivoTxt(string codigo, string NombreArchivo)
        {
            string pathTxt = "/sdcard/Download/" + NombreArchivo;

            StringBuilder constructor = new StringBuilder();

            String CodigoTxt = codigo + "\r\n";

            if (!File.Exists(pathTxt))
            {
                StreamWriter streamWriter = File.CreateText(pathTxt);
                constructor.AppendLine(CodigoTxt);
                streamWriter.Dispose();
            }
            else
            {
                constructor.AppendLine(CodigoTxt);
            }
            File.AppendAllText(pathTxt, constructor.ToString());
        }
    }
}