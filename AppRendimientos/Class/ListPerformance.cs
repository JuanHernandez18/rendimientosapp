﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppRendimientos.Class
{
    class ListPerformance
    {
        int _BusinnesUnitID;
        int _PlantProductID;
        int _ProductID;
        int _ColourID;
        int _QtyStemID;
        int _QualityID;
        string _Collaborator;
        string _Date;


        public ListPerformance(
                  int BusinnesUnitID,
                  int PlantProductID,
                  int ProductID,
                  int ColourID,
                  int QtyStemID,
                  int QualityID,
                  string Collaborator,
                  string Date)
        {

            this._BusinnesUnitID = BusinnesUnitID;
            this._PlantProductID = PlantProductID;
            this._ProductID = ProductID;
            this._ColourID = ColourID;
            this._QtyStemID = QtyStemID;
            this._QualityID = QualityID;
            this._Collaborator = Collaborator;
            this._Date = Date;
        }

        public int busBusinnesUnitID
        {
            get
            {
                return _BusinnesUnitID;
            }
            set
            {
                _BusinnesUnitID = value;
            }
        }

        public int PlantProductID
        {
            get
            {
                return _PlantProductID;
            }
            set
            {
                _PlantProductID = value;
            }
        }

        public int ProductID
        {
            get
            {
                return _ProductID;
            }
            set
            {
                _ProductID = value;
            }
        }

        public int ColourID
        {
            get
            {
                return _ColourID;
            }
            set
            {
                _ColourID = value;
            }
        }

        public int QtyStemID
        {
            get
            {
                return _QtyStemID;
            }
            set
            {
                _QtyStemID = value;
            }
        }

        public int QualityID
        {
            get
            {
                return _QualityID;
            }
            set
            {
                _QualityID = value;
            }
        }

        public string Collaborator
        {
            get
            {
                return _Collaborator;
            }
            set
            {
                _Collaborator = value;
            }
        }

        public string Date
        {
            get
            {
                return _Date;
            }
            set
            {
                _Date = value;
            }
        }
    }

}