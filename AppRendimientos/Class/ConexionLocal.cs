﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.Database.Sqlite;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppRendimientos.Class
{
    class ConexionLocal : SQLiteOpenHelper
    {
        private static String dbName = "AppRendimientos.bd";

        
        //Variables postc
        public static string BusinessUnitID = "BusinessUnitID";
        public static string Name = "Name";
        private static string BusinessUnit = "BusinessUnit";

        //Parametros
        private static string Parameters = "Parameters";


        //Variables Calidad
        public static string QualityID = "QualityID";
        public static string PlantProductID = "PlantProductID";
        private static string Quality = "Quality";


        //Variables Cantidad
        public static string QtyStemID = "QtyStemID";
        public static string Quantity = "Quantity";
        private static string QtyStem = "QtyStem";


        //Variables clases productos
        private static string PlantProduct = "PlantProduct";

        //Variades AppRendimientos
        public static string PerformanceID = "PerformanceID";
        public static string Collaborator = "Collaborator";
        public static string Date = "Date";
        public static string SyncFlag = "SyncFlag";
        private static string Performance = "Performance";

        //Variables Cantidad
        public static string BouquetTypeID = "BouquetTypeID";
        private static string BouquetType = "BouquetType";

        //Variades Hora Operación 
        public static string OperationStartDatetimeID = "OperationStartDatetimeID";
        public static string OperationStart = "OperationStart";       
        private static string OperationStartDatetime = "OperationStartDatetime";

        public ConexionLocal(Context Context) : base(Context, dbName, null, 1)
        {
        }
        public override void OnCreate(SQLiteDatabase db)
        {
            db.ExecSQL("CREATE TABLE " + BusinessUnit +
              " (" + BusinessUnitID + " INTEGER, "
                   + Name + " TEXT )");

            //Parametros
            db.ExecSQL("CREATE TABLE " + Parameters +
              " (" + BusinessUnitID + " INTEGER, "
                   + PlantProductID + " INTEGER," 
                   + BouquetTypeID + " INTEGER )");

            //ClasesProductos
            db.ExecSQL("CREATE TABLE " + PlantProduct +
              " (" + PlantProductID + " INTEGER, "
                   + Name + " TEXT )");

            //Calidad
            db.ExecSQL("CREATE TABLE " + Quality +
              " (" + QualityID + " INTEGER, "
                   + Name + " TEXT, "
                   + PlantProductID + " INTEGER )");


            //Cantidad Tallos
            db.ExecSQL("CREATE TABLE " + QtyStem +
              " (" + QtyStemID + " INTEGER, "
                   + Name + " TEXT, "
                   + Quantity + " INTEGER, "
                   + PlantProductID + " INTEGER )");


            //Rendimiento
            db.ExecSQL("CREATE TABLE " + Performance +
              " (" + PerformanceID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
                   + BusinessUnitID + " INTEGER, "
                   + PlantProductID + " INTEGER, "
                   + Collaborator + " TEXT, "
                   + QtyStemID + " INTEGER, "
                   + QualityID + " INTEGER, "
                   + Date + " TEXT, " 
                   + BouquetTypeID + " INTEGER, "
                   + SyncFlag + " INTEGER )");

            //Tipo
            db.ExecSQL("CREATE TABLE " + BouquetType +
              " (" + BouquetTypeID + " INTEGER, "
                   + Name + " TEXT )");

            //Fecha y hora operación 
            db.ExecSQL("CREATE TABLE " + OperationStartDatetime +
             " (" + OperationStartDatetimeID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                  + BusinessUnitID + " INTEGER, "
                  + BouquetTypeID + " INTEGER, "
                  + OperationStart + " TEXT," 
                  + SyncFlag + " INTEGER )");
        }


        public override void OnUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
        }

        //INSERT
        public void SaveBusinessUnit(int txtBusinessUnitId, string txtName)
        {
            ContentValues saveData = new ContentValues();
            saveData.Put(BusinessUnitID, txtBusinessUnitId);
            saveData.Put(Name, txtName);
            this.WritableDatabase.Insert(BusinessUnit, null, saveData);
        }

        public void SaveParameters(int txtBusinessUnit, int txtPlantProductID, int txtBouquetTypeID)
        {
            ContentValues saveData = new ContentValues();
            saveData.Put(PlantProductID, txtPlantProductID);
            saveData.Put(BusinessUnitID, txtBusinessUnit);
            saveData.Put(BouquetTypeID, txtBouquetTypeID);
            this.WritableDatabase.Insert(Parameters, null, saveData);
        }

        public void SavePlantProduct(int txtPlantProductID, string txtName)
        {
            ContentValues saveData = new ContentValues();
            saveData.Put(PlantProductID, txtPlantProductID);
            saveData.Put(Name, txtName);
            this.WritableDatabase.Insert(PlantProduct, null, saveData);
        }
        
        public void SaveQuality(int txtQualityID, string txtName, int txtPlantProductID)
        {
            ContentValues saveData = new ContentValues();
            saveData.Put(QualityID, txtQualityID);
            saveData.Put(Name, txtName);
            saveData.Put(PlantProductID, txtPlantProductID);
            this.WritableDatabase.Insert(Quality, null, saveData);
        }

        public void SaveQtyStem(int txtQtyStemID, string txtName, int txtQuantity, int txtPlantProductID)
        {
            ContentValues saveData = new ContentValues();
            saveData.Put(QtyStemID, txtQtyStemID);
            saveData.Put(Name, txtName);
            saveData.Put(Quantity, txtQuantity);
            saveData.Put(PlantProductID, txtPlantProductID);
            this.WritableDatabase.Insert(QtyStem, null, saveData);
        }

        public void SavePerformance(int txtBusinessUnitID, int txtPlantProductID,string txtCollaborator,int txtQtyStemID, int txtQualityID,string txtDate, int txtBouquetTypeID, int txtSyncFlag)
        {
            ContentValues saveData = new ContentValues();
            saveData.Put(BusinessUnitID, txtBusinessUnitID);
            saveData.Put(PlantProductID, txtPlantProductID);
            saveData.Put(Collaborator, txtCollaborator);
            saveData.Put(QtyStemID, txtQtyStemID);
            saveData.Put(QualityID, txtQualityID);
            saveData.Put(Date, txtDate);
            saveData.Put(BouquetTypeID, txtBouquetTypeID);
            saveData.Put(SyncFlag, txtSyncFlag);
            this.WritableDatabase.Insert(Performance, null, saveData);
        }

        public void SaveBouquetType(int txtBouquetTypeID, string txtName)
        {
            ContentValues saveData = new ContentValues();
            saveData.Put(BouquetTypeID, txtBouquetTypeID);
            saveData.Put(Name, txtName);
            this.WritableDatabase.Insert(BouquetType, null, saveData);
        }

        public void SaveOperationStartDatetime(int txtBusinessUnitID, int txtBouquetTypeID, string txtOperation)
        {
            ContentValues saveData = new ContentValues();
            saveData.Put(BusinessUnitID, txtBusinessUnitID);
            saveData.Put(BouquetTypeID, txtBouquetTypeID);
            saveData.Put(OperationStart, txtOperation);
            saveData.Put(SyncFlag, 0);
            this.WritableDatabase.Insert(OperationStartDatetime, null, saveData);
        }

       
        //Query
        public ICursor queryBusinessUnit()
        {
            String query = "SELECT * FROM BusinessUnit ORDER BY Name";
            ICursor Tablas = this.WritableDatabase.RawQuery(query, null);
            return Tablas;
        }

        public ICursor queryBouquetType()
        {
            String query = "SELECT * FROM BouquetType ORDER BY Name";
            ICursor Tablas = this.WritableDatabase.RawQuery(query, null);
            return Tablas;
        }
        public ICursor queryPlantProduct()
        {
            String query = "SELECT * FROM PlantProduct ORDER BY Name";
            ICursor Tablas = this.WritableDatabase.RawQuery(query, null);
            return Tablas;
        }
        public ICursor queryParameters()
        {
            String query = "SELECT BU.Name, BT.Name, P.BusinessUnitID, P.BouquetTypeID FROM Parameters P INNER JOIN BusinessUnit BU ON BU.BusinessUnitID = P.BusinessUnitID INNER JOIN BouquetType BT ON BT.BouquetTypeID = P.BouquetTypeID";
            ICursor Tablas = this.WritableDatabase.RawQuery(query, null);
            return Tablas;
        }

        public ICursor queryQtyStem(int PlantProductID)
        { 
            String query = "SELECT * FROM QtyStem  WHERE PlantProductID = " + PlantProductID + " ORDER BY Quantity";
            ICursor Tablas = this.WritableDatabase.RawQuery(query, null);
            return Tablas;
        }
        
        public ICursor queryQuality(int PlantProductID)
        {
            String query = "SELECT * FROM Quality WHERE PlantProductID = " + PlantProductID + " ORDER BY Name";
            ICursor Tablas = this.WritableDatabase.RawQuery(query, null);
            return Tablas;
        }
        public ICursor querySelected(int plantProductID, int qualityID, int colourID, int productID, int qtyStemID)
        {
            String query = "SELECT PP.Name, Q.Name, C.Name, P.Name, QS.Name FROM PlantProduct PP, Quality Q, Colour C, Product P, QtyStem QS WHERE PP.PlantProductID = " + plantProductID + " AND Q.QualityID = " + qualityID + " AND C.ColourID = " + colourID + " AND P.ProductID = " + productID + " AND QS.QtyStemID = " + qtyStemID;
            ICursor Tablas = this.WritableDatabase.RawQuery(query, null);
            return Tablas;
        }

        public ICursor getParameterUnit()
        {
            String query = "SELECT  DISTINCT BusinessUnitID, PlantProductID, BouquetTypeID FROM Parameters";
            ICursor Tablas = this.WritableDatabase.RawQuery(query, null);
            return Tablas;
        }

        public ICursor getParameterProduct()
        {
            String query = "SELECT P.PlantProductID, PL.Name FROM Parameters P INNER JOIN PlantProduct PL ON PL.PlantProductID = P.PlantProductID ORDER BY Name";

            ICursor Tablas = this.WritableDatabase.RawQuery(query, null);
            return Tablas;
        }

        public ICursor querySync()
        {
            String query = "SELECT * FROM Performance WHERE SyncFlag = 0";
            ICursor Tablas = this.WritableDatabase.RawQuery(query, null);
            return Tablas;
        }

        public ICursor querySyncOperation()
        {
            String query = "SELECT * FROM  OperationStartDatetime WHERE SyncFlag = 0";
            ICursor Tablas = this.WritableDatabase.RawQuery(query, null);
            return Tablas;
        }

        

        //Update
        public void UpdateSync(int PerformanceID)
        {
            ContentValues valTask = new ContentValues();
            valTask.Put(SyncFlag, 1);
            string where = "PerformanceID = " + PerformanceID;
            this.WritableDatabase.Update(Performance, valTask, where, null);
        }

         public void UpdateSyncOperations(int OperationsID)
        {
            ContentValues valTask = new ContentValues();
            valTask.Put(SyncFlag, 1);
            string where = "OperationStartDatetimeID = " + OperationsID;
            this.WritableDatabase.Update(OperationStartDatetime, valTask, where, null);
        }
        
        //Delete
        public void DeletePlantProduct()
        {
            String where = "";
            this.WritableDatabase.Delete(PlantProduct, where, null);
        }
        
        public void DeleteQuality()
        {
            String where = "";
            this.WritableDatabase.Delete(Quality, where, null);
        }
        public void DeleteQtyStem()
        {
            String where = "";
            this.WritableDatabase.Delete(QtyStem, where, null);
        }

        public void DeleteBusinessUnit()
        {
            String where = "";
            this.WritableDatabase.Delete(BusinessUnit, where, null);
        }
        public void DeleteBuoquetType()
        {
            String where = "";
            this.WritableDatabase.Delete(BouquetType, where, null);
        }

        public void DeleteParameters()
        {
            String where = "";
            this.WritableDatabase.Delete(Parameters, where, null);
        }
    }
}