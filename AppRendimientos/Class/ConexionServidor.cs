﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace AppRendimientos.Class
{
    class ConexionServidor
    {
        SqlConnection conexion;
        ConexionLocal db;
        String addressIPLocal;
        String database;
        String user;
        String password;
        String ConexionLocal;

        private void validateConexion()
        {
            addressIPLocal = "10.99.0.2";
            database = "BAS";
            user = "PRaut";
            password = "Neon001*";

            ConexionLocal = @"data source= " + addressIPLocal + ";initial catalog=" + database + ";user id=" + user + ";password= " + password + ";Connect Timeout=0;"; //;Max Pool Size=50000 Max Pool Size=200;MultipleActiveResultSets=true

            conexion = new SqlConnection(ConexionLocal);

            try
            {
                conexion.Open();
            }
            catch (SqlException Exp)
            {
                Toast.MakeText(Application.Context, "No estas conectado a internet", ToastLength.Long).Show();
            }

        }

        public SqlDataReader getPlantProduct()
        {
            validateConexion();

            string sqlData = "SELECT * FROM PlantProduct";
            SqlCommand Data = new SqlCommand(sqlData, conexion);
            SqlDataReader AppData;

            AppData = Data.ExecuteReader();
            return AppData;
        }

        public SqlDataReader getProduct()
        {
            validateConexion();

            string sqlData = "SELECT * FROM Product";
            SqlCommand Data = new SqlCommand(sqlData, conexion);
            SqlDataReader AppData;

            AppData = Data.ExecuteReader();
            return AppData;
        }

        public SqlDataReader getQuality()
        {
            validateConexion();

            string sqlData = "SELECT * FROM Quality";
            SqlCommand Data = new SqlCommand(sqlData, conexion);
            SqlDataReader AppData;

            AppData = Data.ExecuteReader();
            return AppData;
        }

        public SqlDataReader getQtyStem()
        {
            validateConexion();

            string sqlData = "SELECT * FROM QtyStem";
            SqlCommand Data = new SqlCommand(sqlData, conexion);
            SqlDataReader AppData;

            AppData = Data.ExecuteReader();
            return AppData;
        }

        public SqlDataReader getQtyBqt()
        {
            validateConexion();

            string sqlData = "SELECT * FROM QtyBqt";
            SqlCommand Data = new SqlCommand(sqlData, conexion);
            SqlDataReader AppData;

            AppData = Data.ExecuteReader();
            return AppData;
        }


        public SqlDataReader getColour()
        {
            validateConexion();

            string sqlData = "SELECT * FROM Colour";
            SqlCommand Data = new SqlCommand(sqlData, conexion);
            SqlDataReader AppData;

            AppData = Data.ExecuteReader();
            return AppData;
        }

        public SqlDataReader getBusinessUnit()
        {
            validateConexion();

            string sqlData = "SELECT * FROM BusinessUnit";
            SqlCommand Data = new SqlCommand(sqlData, conexion);
            SqlDataReader AppData;

            AppData = Data.ExecuteReader();
            return AppData;
        }

        public SqlDataReader getBuoquetType()
        {
            validateConexion();

            string sqlData = "SELECT * FROM BouquetType WHERE FlagActive = 1";
            SqlCommand Data = new SqlCommand(sqlData, conexion);
            SqlDataReader AppData;

            AppData = Data.ExecuteReader();
            return AppData;
        }

    }
}